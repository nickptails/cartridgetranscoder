#include "gst/gst.h"


/** Set the transcoder running.
 * @param pipe The pipeline for transcoding.
 * @param loop The main loop of the calling application.
 */
gboolean transcoder_run(GstElement* pipe, GMainLoop* loop);

/** Release all resources associated to transcoder.
 * @param pipe The pipeline for transcoding.
 */
gboolean transcoder_free(GstElement* pipe);
