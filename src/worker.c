#include "stdio.h"
#include "gst/gst.h"


gboolean bus_handle (GstBus* bus, GstMessage* msg, gpointer data) {
	GMainLoop* loop = (GMainLoop*) data;
	switch (GST_MESSAGE_TYPE(msg)) {
        case GST_MESSAGE_ERROR: {
            GError* error;
            gchar* debug;
            gst_message_parse_error (msg, &error, &debug);
            g_free (debug);
            fprintf(stderr, "Error in transcoding: %s\n", error->message);
            g_error_free (error);
        }
        case GST_MESSAGE_EOS:
            g_main_loop_quit(loop);
            break;
        default:
            break;
	}
    return TRUE;
}


gboolean transcoder_run(GstElement* pipe, GMainLoop* loop) {
    GstBus* bus = gst_pipeline_get_bus(GST_PIPELINE(pipe));
    gst_bus_add_watch(bus, bus_handle, loop);
    gst_object_unref(bus);
    if (gst_element_set_state(pipe, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "Unable to set the pipeline running..\n");
        return FALSE;
    }
    return TRUE;
}

gboolean transcoder_free(GstElement* pipe) {
    if (gst_element_set_state(pipe, GST_STATE_NULL) == GST_STATE_CHANGE_FAILURE) {
        fprintf(stderr, "Unable to release transcoding pipeline..\n");
        return FALSE;
    }
    gst_object_unref(pipe);
    return TRUE;
}
