#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "gst/gst.h"
#include "gst/pbutils/pbutils.h"
#include "tcoder.h"


GstEncodingProfile* transcoder_find_profile (char* name) {
    GstEncodingProfile* profile;
    char* tname;
    tname = strsep(&name, TARGET_PROFILE_SEP);
    profile = gst_encoding_profile_find(tname, name, NULL);
    if (profile == NULL) {
        fprintf(stderr, "Unable to find profile %s\n", name);
        return NULL;
    }
    return profile;
}

GstElement* transcoder_create_pipeline(const char* pipename) {
    GstElement* pipe = gst_pipeline_new(pipename), *reader, *tcoder, *writer;
    reader = gst_element_factory_make("fdsrc", INPUT_ELEMENT_NAME);
    tcoder = gst_element_factory_make("transcodebin", TRANSCODER_ELEMENT_NAME);
    writer = gst_element_factory_make("fdsink", OUTPUT_ELEMENT_NAME);
    if (reader == NULL || tcoder == NULL || writer == NULL) {
        fprintf(stderr, "Unable to create pipeline element..\n");
        return NULL;
    }
    gst_bin_add_many(GST_BIN(pipe), reader, tcoder, writer, NULL);
    if (!gst_element_link_many(reader, tcoder, writer, NULL)) {
        fprintf(stderr, "Unable to link elements in pipeline..\n");
        return NULL;
    }
    return pipe;
}

gboolean transcoder_set_io_files (GstElement* pipe, int fds[2]) {
    GstElement* reader, *writer;
    /* verify file descriptors */
    if (fds[0] < 0 || fds[1] < 0) {
        fprintf(stderr, "Invalid file descriptor given..\n");
        return FALSE;
    }
    reader = gst_bin_get_by_name(GST_BIN(pipe), INPUT_ELEMENT_NAME);
    writer = gst_bin_get_by_name(GST_BIN(pipe), OUTPUT_ELEMENT_NAME);
    if (reader == NULL || writer == NULL) {
        fprintf(stderr, "Unable to retrieve IO element..\n");
        if (reader != NULL)
            gst_object_unref(reader);
        if (writer != NULL)
            gst_object_unref(writer);
        return FALSE;
    }
    g_object_set(reader, "fd", fds[0], NULL);
    g_object_set(writer, "fd", fds[1], NULL);
    gst_object_unref(reader);
    gst_object_unref(writer);
    return TRUE;
}

gboolean transcoder_set_profile (GstElement* pipe, GstEncodingProfile* prof) {
    GstElement* tcoder;
    /* verify profile for transcoding */
    if (prof == NULL) {
        fprintf(stderr, "No profile given..\n");
        return FALSE;
    }
    tcoder = gst_bin_get_by_name(GST_BIN(pipe), TRANSCODER_ELEMENT_NAME);
    if (tcoder == NULL) {
        fprintf(stderr, "Unable to retrieve transcoder element..\n");
        return FALSE;
    }
    g_object_set(tcoder, "profile", prof, NULL);
    gst_object_unref(tcoder);
    return TRUE;
}
