#include "stdlib.h"
#include "stdio.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "unistd.h"
#include "errno.h"
#include "gst/gst.h"
#include "tcoder.h"
#include "worker.h"

#define TRANSCODER_PIPELINE_NAME "cdgtcoder"


static gchar* ipath = NULL, *opath = NULL, *prof = NULL;
static gboolean owr = FALSE;

static GOptionEntry entries[] =
{
  { "input", 'i', 0, G_OPTION_ARG_FILENAME, &ipath, "Path for input file", "PATH" },
  { "output", 'o', 0, G_OPTION_ARG_FILENAME, &opath, "Path for output file", "PATH" },
  { "profile", 'p', 0, G_OPTION_ARG_STRING, &prof, "Encoding profile", "TARGET/PROFILE" },
  { "force", 'f', 0, G_OPTION_ARG_NONE, &owr, "Overwrite output file", NULL},
  { NULL }
};

struct transcoder_data {
    GstElement* pipe;
    int fds[2];
    GstEncodingProfile* prof;
    GMainLoop* loop;
};

gboolean parse_args (int argc, char** argv) {
    GOptionContext* context;
    GError* error = NULL;
    gchar* help;
    context = g_option_context_new ("Transcode files");
    g_option_context_add_main_entries (context, entries, NULL);
    g_option_context_add_group (context, gst_init_get_option_group());
    if (!g_option_context_parse (context, &argc, &argv, &error))
    {
        fprintf(stderr, "Error while parsing: %s\n", error->message);
        g_error_free(error);
        return FALSE;
    }
    if (ipath == NULL || opath == NULL || prof == NULL) {
        help = g_option_context_get_help(context, TRUE, NULL);
        fprintf(stderr, "%s\nMissing option data..\n", help);
        g_free(help);
        g_option_context_free(context);
        return FALSE;
    }
    g_option_context_free(context);
    return TRUE;
}

gboolean open_files (int fds[2]) {
    int oflags = O_RDWR | O_CREAT | O_TRUNC;
    if (!owr)
        oflags |= O_EXCL;
    fds[0] = open(ipath, O_RDONLY);
    if (fds[0] == -1) {
        fprintf(stderr, "Unable to open input file: %s\n", ipath);
        return FALSE;
    }
    fds[1] = open(opath, oflags, 0644);
    if (fds[1] == -1) {
        if (errno == EEXIST)
            fprintf(stderr, "File already exists. Type -f to overwrite..\n");
        else
            fprintf(stderr, "Unable to open output file: %s\n", opath);
        close(fds[0]);
        return FALSE;
    }
    return TRUE;
}

gboolean transcode(struct transcoder_data* tdata) {
    tdata -> pipe = transcoder_create_pipeline(TRANSCODER_PIPELINE_NAME);
    if (tdata -> pipe == NULL) {
        fprintf(stderr, "Failed to create a pipeline for transcoding..\n");
        return FALSE;
    }
    if (!transcoder_set_io_files(tdata -> pipe, tdata -> fds) ||
            !transcoder_set_profile(tdata -> pipe, tdata -> prof)) {
        fprintf(stderr, "Unable to set data for transcoding..\n");
        return FALSE;
    }
    if (!transcoder_run(tdata -> pipe, tdata->loop)) {
        fprintf(stderr, "Unable to set up pipeline for transcoding..\n");
        return FALSE;
    }
    return TRUE;
}

int main(int argc, char* argv[]) {
    struct transcoder_data tdata;
    memset(&tdata, 0, sizeof(struct transcoder_data));
    tdata.loop = g_main_loop_new(NULL, FALSE);
    if (!parse_args(argc, argv) || !open_files(tdata.fds))
        return EXIT_FAILURE;
    tdata.prof = transcoder_find_profile(prof);
    if (tdata.prof == NULL) {
        fprintf(stderr, "Unable to retrieve profile %s\n", prof);
        return EXIT_FAILURE;
    }
    if (!transcode(&tdata)) {
        fprintf(stderr, "Error while setting up transcoder..\n");
        return EXIT_FAILURE;
    }
    g_main_loop_run(tdata.loop);
    if (!transcoder_free(tdata.pipe)) {
        fprintf(stderr, "Unable to free transcoder resources..\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
