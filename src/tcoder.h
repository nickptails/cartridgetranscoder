#include "gst/gst.h"
#include "gst/pbutils/pbutils.h"


#define INPUT_ELEMENT_NAME "reader"
#define TRANSCODER_ELEMENT_NAME "transcoder"
#define OUTPUT_ELEMENT_NAME "writer"

#define TARGET_PROFILE_SEP "/"


/** Retrieve profile for transcoding.
 * @param name Profile identifier (eventually preceded by target name).
 *
 * Retrieve profile from GStreamer framework identified by the given name,
 * so that it may be used by the transcoder.
 */
GstEncodingProfile* transcoder_find_profile (char* name);

/** Create pipeline for transcoding.
 * @param pipename Name for the created pipeline.
 *
 * Make a new pipeline with the given name, containing the elements
 * suited for transcoding (required information has to be set
 * on the right elements before setting it running).
 */
GstElement* transcoder_create_pipeline(const char* pipename);

/** Set input and output files for transcoding.
 * @param pipe Transcoding pipeline.
 * @param fds Array of descriptors for involved files.
 */
gboolean transcoder_set_io_files (GstElement* pipe, int fds[2]);

/** Set profile for transcoding.
 * @param pipe Transcoding pipeline.
 * @param prof Desired encoding profile.
 */
gboolean transcoder_set_profile (GstElement* pipe, GstEncodingProfile* prof);
